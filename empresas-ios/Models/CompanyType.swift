//
//  CompanyType.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 13/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import Foundation


struct CompanyType: Codable {
    
    var id: Int? = nil
    var name: String? = nil

    enum CodingKeys: String, CodingKey {
        case id
        case name = "enterprise_type_name"
    }
    
}
