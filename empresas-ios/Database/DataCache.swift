//
//  DataCache.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import Foundation

struct DataCache {
    
    static fileprivate let userDefaults = UserDefaults.standard
    
    static func saveSession(dict: [String: String]?) -> Bool {
        guard let accessToken = dict?["access-token"] else { return false }
        guard let client = dict?["client"] else { return false }
        guard let uid = dict?["uid"] else { return false }
        
        userDefaults.set(accessToken, forKey: "access_token")
        userDefaults.set(client, forKey: "client")
        userDefaults.set(uid, forKey: "uid")
        
        return true
    }
    
    static func getSession() -> [String: String]? {
        guard let accessToken = userDefaults.string(forKey: "access_token") else { return nil }
        guard let client = userDefaults.string(forKey: "client") else { return nil }
        guard let uid = userDefaults.string(forKey: "uid") else { return nil }
        return ["access-token": accessToken, "client": client, "uid": uid]
    }
    
    static func clear() -> Bool {
        guard let domain = Bundle.main.bundleIdentifier else { return false }
        userDefaults.removePersistentDomain(forName: domain)
        return userDefaults.synchronize()
    }
    
}
