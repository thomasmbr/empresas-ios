//
//  Enterprise.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 13/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import Foundation
import UIKit

struct Enterprise: Codable {
    
    var name: String? = nil
    var companyType: CompanyType? = nil
    var description: String? = nil
    var country: String? = nil
    var photo: String? = nil
    var color: UIColor? = nil
    
    enum CodingKeys: String, CodingKey {
        case name = "enterprise_name"
        case companyType = "enterprise_type"
        case description
        case country
        case photo
    }
    
}
