//
//  Login.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import Foundation


struct Login: Encodable {
    
    let email: String
    let password: String
    
}
