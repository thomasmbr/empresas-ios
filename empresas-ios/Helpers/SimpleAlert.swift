//
//  SimpleAlert.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit

class SimpleAlert {
    
    var title: String?
    var message: String?
    var cancelButton: Bool
    var actionButtonText: String
    
    init(title: String?, message: String?, cancelButton: Bool = false, actionButtonText: String = "Ok") {
        self.title = title
        self.message = message
        self.cancelButton = cancelButton
        self.actionButtonText = actionButtonText
    }
    
    func present(viewController: UIViewController, onCancel: (() -> Void)? = nil, onAction: (() -> Void)? = nil, onCompletion: (() -> Void)? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if cancelButton {
            let actionCancel = UIAlertAction(title: UIViewController.string("cancel"), style: .cancel) { (_) in
                onCancel?()
            }
            alert.addAction(actionCancel)
        }
        let action = UIAlertAction(title: actionButtonText, style: .default) { (_) in
            onAction?()
        }
        alert.addAction(action)
        viewController.present(alert, animated: true) {
            onCompletion?()
        }
    }
    
}

