//
//  HomeViewController.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Kingfisher

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalEnterprisesFoundLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    let searchController = UISearchController(searchResultsController: nil)
    var enterprises = [Enterprise]()
    var loadingViewIndicator: NVActivityIndicatorView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setupTableView()
        setupLoadingViewIndicator()
        setupSearchController()
    }
    
    @IBAction func didTapLogout(_ sender: UIBarButtonItem) {
        SimpleAlert(
            title: string("warning"),
            message: string("message_logout"),
            cancelButton: true,
            actionButtonText: string("yes")
        ).present(viewController: self, onCancel: {}, onAction: {
            Network.logout(onSuccess: {
                self.goToLogin()
            }) { (error) in
                SimpleAlert(title: self.string("warning"), message: error).present(viewController: self)
            }
        }) { }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "enterpriseDetailSegue":
            let enterpriseDetailVC = segue.destination as? EnterpriseDetailViewController
            guard let selectedRow = tableView.indexPathForSelectedRow?.row else { return }
            enterpriseDetailVC?.enterprise = enterprises[selectedRow]
            searchController.searchBar.text = ""
            break
        default: break
        }
    }
    
    func goToLogin() {
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {
          fatalError("could not get scene delegate ")
        }
        sceneDelegate.goToLogin()
    }
    
    func setupNavigationBar() {
        navigationController?.navigationBar.isTranslucent = false
        let appearance = UINavigationBarAppearance()
        appearance.configureWithDefaultBackground()
        appearance.backgroundColor = UIColor(named: "bgcolor_header_home")
        appearance.titleTextAttributes = [.foregroundColor: UIColor.lightText]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance
    }
    
    func setupTableView() {
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupLoadingViewIndicator() {
        let frame = CGRect(x: 0, y: 0, width: 100.0, height: 100.0)
        let colorIndicator = UIColor(named: "color_loading_indicator_view")!
        loadingViewIndicator = NVActivityIndicatorView(frame: frame, type: .ballClipRotateMultiple, color: colorIndicator, padding: nil)
        loadingViewIndicator?.center = self.view.center
        self.view.addSubview(loadingViewIndicator!)
    }
    
    func setupSearchController() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = string("search_enterprises")
        searchController.searchBar.showsCancelButton = false
        navigationItem.searchController = searchController
        definesPresentationContext = true
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.searchTextField.backgroundColor = UIColor.white
    }
    
    func getAllEnterprises() {
        hideTotalFoundMessage()
        loadingViewIndicator?.startAnimating()
        Network.getEnterprises(onSuccess: { listEnterprises in
            DispatchQueue.main.async {
                self.loadingViewIndicator?.stopAnimating()
                self.enterprises = listEnterprises
                self.tableView.reloadData()
            }
        }) { (error) in
            DispatchQueue.main.async {
                self.loadingViewIndicator?.stopAnimating()
            }
        }
    }
    
    func getFilteredEnterprises(_ text: String) {
        hideTotalFoundMessage()
        loadingViewIndicator?.startAnimating()
        Network.getEnterprisesBy(name: text, onSuccess: { listEnterprises in
            DispatchQueue.main.async {
                self.loadingViewIndicator?.stopAnimating()
                self.enterprises = listEnterprises
                self.tableView.isHidden = false
                self.tableView.reloadData()
                self.showTotalFoundMessage()
            }
        }) { (error) in
            DispatchQueue.main.async {
                self.loadingViewIndicator?.stopAnimating()
            }
        }
    }
    
    func showTotalFoundMessage() {
        if enterprises.isEmpty {
            totalEnterprisesFoundLabel.isHidden = true
            messageLabel.isHidden = false
            messageLabel.text = string("no_results_found")
        } else if enterprises.count < 10 {
            totalEnterprisesFoundLabel.isHidden = false
            if enterprises.count == 1 {
                totalEnterprisesFoundLabel.text = "0\(enterprises.count) \(string("result_found"))"
            } else {
                totalEnterprisesFoundLabel.text = "0\(enterprises.count) \(string("results_found"))"
            }
        } else {
            totalEnterprisesFoundLabel.isHidden = false
            totalEnterprisesFoundLabel.text = "\(enterprises.count) \(string("results_found"))"
        }
    }
    
    func showError(message: String) {
        tableView.isHidden = true
        messageLabel.isHidden = false
        messageLabel.text = message
    }
    
    func hideTotalFoundMessage() {
        totalEnterprisesFoundLabel.isHidden = true
        totalEnterprisesFoundLabel.text = ""
        messageLabel.isHidden = true
        messageLabel.text = ""
    }
    
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "enterpriseDetailSegue", sender: self)
    }
    
}

extension HomeViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return enterprises.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "enterpriseCell") as? EnterpriseTableViewCell {
            let enterprise = enterprises[indexPath.row]
            cell.nameLabel.text = enterprise.name
            cell.bgView.backgroundColor = enterprise.color
            if let photo = enterprise.photo {
                let urlImage = Network.getPhotoUrl(photoPath: photo)
                cell.logoImageView.layer.cornerRadius = 4
                cell.logoImageView.layer.masksToBounds = true
                cell.logoImageView.kf.setImage(with: urlImage, placeholder: UIImage(named: "enterprise_placeholder")) { result in
                    switch result {
                    case .success:
                        break
                    case .failure:
                        cell.logoImageView.image = UIImage(named: "image_broken")
                        break
                    }
                }
            }
            return cell
        }
        return UITableViewCell()
    }
    
}

extension HomeViewController: UISearchResultsUpdating, UISearchBarDelegate {
    
    func updateSearchResults(for searchController: UISearchController) {
        guard let text = searchController.searchBar.text else { return }
        if text.count > 0 {
            getFilteredEnterprises(text)
        } else {
            enterprises = []
            tableView.reloadData()
            tableView.isHidden = true
            showTotalFoundMessage()
        }
    }
    
}
