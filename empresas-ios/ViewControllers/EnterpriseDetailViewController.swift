//
//  EnterpriseDetailViewController.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 14/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit

class EnterpriseDetailViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var bgHeaderView: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: VerticalTopAlignLabel!
    
    var enterprise: Enterprise?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNavigationBar()
        setEnterpriseInfo()
    }
    
    func setupNavigationBar() {
        let appearance = UINavigationBarAppearance()
        appearance.configureWithOpaqueBackground()
        appearance.backgroundColor = UIColor.white
        appearance.titleTextAttributes = [.foregroundColor: UIColor.lightText]
        navigationItem.standardAppearance = appearance
        navigationItem.scrollEdgeAppearance = appearance
        navigationItem.compactAppearance = appearance
        let backBTN = UIBarButtonItem(image: UIImage(named: "back_button"),
                                      style: .plain,
                                      target: navigationController,
                                      action: #selector(UINavigationController.popViewController(animated:)))
        navigationItem.leftBarButtonItem = backBTN
    }
    
    func setEnterpriseInfo() {
        guard let info = enterprise else { return }
        bgHeaderView.backgroundColor = info.color
        nameLabel.text = info.name
        imageView.kf.setImage(with: Network.getPhotoUrl(photoPath: info.photo), placeholder: UIImage(named: "enterprise_placeholder")) { result in
            switch result {
            case .success:
                break
            case .failure:
                self.imageView.image = UIImage(named: "image_broken")
                break
            }
        }
        if let description = info.description, !description.isEmpty {
            descriptionLabel.text = description
        } else {
            descriptionLabel.text = string("no_description")
        }
    }

}
