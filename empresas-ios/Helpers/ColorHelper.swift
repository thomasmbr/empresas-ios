//
//  ColorHelper.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 13/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit

class ColorHelper {
    
    fileprivate static let colors = [
        UIColor(hexString: "#EF5350"),
        UIColor(hexString: "#EC407A"),
        UIColor(hexString: "#BA68C8"),
        UIColor(hexString: "#9575CD"),
        UIColor(hexString: "#7986CB"),
        UIColor(hexString: "#1E88E5"),
        UIColor(hexString: "#0288D1"),
        UIColor(hexString: "#0097A7"),
        UIColor(hexString: "#009688"),
        UIColor(hexString: "#43A047"),
        UIColor(hexString: "#558B2F"),
        UIColor(hexString: "#827717"),
        UIColor(hexString: "#F4511E"),
        UIColor(hexString: "#A1887F"),
        UIColor(hexString: "#757575"),
        UIColor(hexString: "#78909C")
    ]
    
    static func randomColor() -> UIColor {
        return colors.randomElement()!
    }
    
}
