//
//  Network.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import Foundation
import Alamofire

class Network {
    
    static fileprivate let baseURL = "https://empresas.ioasys.com.br"
    static fileprivate let apiVersion = "/v1/"

    static fileprivate func loginUrl() -> String {
        return "\(baseURL)/api\(apiVersion)/users/auth/sign_in"
    }
    
    static fileprivate func enterprisesUrl() -> String {
        return "\(baseURL)/api\(apiVersion)/enterprises"
    }
    
    static func getPhotoUrl(photoPath: String?) -> URL? {
        guard let photoUrl = photoPath else { return nil }
        return URL(string: "\(baseURL)\(photoUrl)")
    }
    
    static func makeLogin(_ email: String, _ password: String, onSuccess: @escaping (()-> Void), onError: @escaping((String)-> Void)) {
        let login = Login(email: email, password: password)
        AF.request(
            loginUrl(),
            method: .post,
            parameters: login,
            encoder: JSONParameterEncoder.default
        ).response { response in
            if let error = response.error {
                onError(error.errorDescription ?? "Unexpected error when logging in")
            } else {
            let headers = response.response?.headers.dictionary
                if DataCache.saveSession(dict: headers) {
                    onSuccess()
                } else {
                    onError("Failed to capture user credentials")
                }
            }
        }
    }
    
    static func userIsLoggedIn() -> Bool {
        return DataCache.getSession() != nil
    }
    
    static func logout(onSuccess: @escaping(()-> Void), onError: @escaping((String)-> Void)) {
        if DataCache.clear() {
            onSuccess()
        } else {
            onError("Error logging out the user")
        }
    }
    
    static func getEnterprises(onSuccess: @escaping(([Enterprise])-> Void), onError: @escaping((String)-> Void)) {
        guard let session = DataCache.getSession() else { return }
        let headears = HTTPHeaders(session)
        AF.request(
            enterprisesUrl(),
            method: .get,
            headers: headears
        ).responseJSON { response in
            switch response.result {
            case .success:
                guard let enterpriseData = EnterprisesData(response.data) else {
                    onError(UIViewController.string("failed_find_enterprises"))
                    return
                }
                let enterprises = enterpriseData.enterprises.map { enterprise -> Enterprise in
                    var object = enterprise
                    object.color = ColorHelper.randomColor()
                    return object
                }
                onSuccess(enterpriseData.enterprises)
            case .failure:
                onError(UIViewController.string("failed_find_enterprises"))
            }
        }
    }
    
    static func getEnterprisesBy(name: String, onSuccess: @escaping(([Enterprise])-> Void), onError: @escaping((String)-> Void)) {
        guard let session = DataCache.getSession() else { return }
        let headears = HTTPHeaders(session)
        let param: [String: String] = ["name": name]
        AF.request(
            enterprisesUrl(),
            method: .get,
            parameters: param,
            encoder: URLEncodedFormParameterEncoder.default,
            headers: headears
        ).responseJSON { response in
            switch response.result {
            case .success:
                guard let enterpriseData = EnterprisesData(response.data) else {
                    onError(UIViewController.string("failed_find_enterprises"))
                    return
                }
                let enterprises = enterpriseData.enterprises.map { enterprise -> Enterprise in
                    var object = enterprise
                    object.color = ColorHelper.randomColor()
                    return object
                }
                onSuccess(enterprises)
            case .failure:
                onError(UIViewController.string("failed_find_enterprises"))
            }
        }
    }
    
}
