//
//  EnterpriseTableViewCell.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 13/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit

class EnterpriseTableViewCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
}
