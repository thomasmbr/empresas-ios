//
//  ViewController.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit
import NVActivityIndicatorView


class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var backgroundLoadingView: UIView!
    @IBOutlet weak var loadingIndicatorView: NVActivityIndicatorView!
    
    let email = "testeapple@ioasys.com.br"
    let password = "12341234"

    override func viewDidLoad() {
        super.viewDidLoad()
        emailTextField.delegate = self
        passwordTextField.delegate = self
        setupLoadingIndicatorView()
        emailTextField.text = email
        passwordTextField.text = password
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    @IBAction func didTapLogin(_ sender: UIButton) {
        hideError()
        showLoading()
        guard let email = emailTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        Network.makeLogin(email, password, onSuccess: {
            self.hideLoading()
            self.goToHome()
        }) { (error) in
            self.hideLoading()
            self.showError(error)
        }
    }
    
    func setupLoadingIndicatorView() {
        loadingIndicatorView.type = .ballClipRotateMultiple
        loadingIndicatorView.color = UIColor(named: "color_loading_indicator_view")!
    }
    
    func hideError() {
        errorLabel.isHidden = true
    }
    
    func showError(_ error: String) {
        errorLabel.isHidden = false
        errorLabel.text = error
    }
    
    func showLoading() {
        backgroundLoadingView.isHidden = false
        loadingIndicatorView.startAnimating()
    }
    
    func hideLoading() {
        backgroundLoadingView.isHidden = true
        loadingIndicatorView.stopAnimating()
    }
    
    func goToHome() {
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {
          fatalError("could not get scene delegate ")
        }
        sceneDelegate.goToHome()
    }
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == emailTextField {
            passwordTextField.becomeFirstResponder()
        } else if textField == passwordTextField {
            passwordTextField.resignFirstResponder()
            loginButton.sendActions(for: .touchUpInside)
        }
        return true
    }
    
}

