//
//  EnterprisesData.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 13/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import Foundation

struct EnterprisesData: Codable {

    var enterprises = [Enterprise]()
    
    init?(_ data: Any?) {
        guard let jsonData = data as? Data else { return nil }
        do {
            let enterprisesData = try JSONDecoder().decode(EnterprisesData.self, from: jsonData)
            self = enterprisesData
        } catch let parsingError {
            print(parsingError)
            return nil
        }
    }
    
    enum CodingKeys: String, CodingKey {
        case enterprises
    }
    
}
