//
//  UIViewControllerExt.swift
//  empresas-ios
//
//  Created by Thomás Marques Brandão Reis on 12/09/20.
//  Copyright © 2020 Thomás Marques Brandão Reis. All rights reserved.
//

import UIKit

extension UIViewController {
    
    static func string(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
    func string(_ key: String) -> String {
        return NSLocalizedString(key, comment: "")
    }
    
}
